"use strict";

var stream = require('stream');
var util = require('util');
var Transform = stream.Transform;

class XivLooper extends stream.Transform {
  constructor(options) {
    super({});

    this.options = util._extend({
      bitDepth: 32,
      channels: 2,
      loopAmount: 2,
      fadeDuration: 10,
      sampleRate: 44100
    }, options);

    this.bytesPerSample = options.channels * (options.bitDepth / 8);
    this.unloopable = options.loopEnd == 0 && options.loopStart == 0;
    this.bufferedBytes = 0;
    this.buffers = [];
  }
  _transform(chunk, enc, cb) {
    var buffer = Buffer.isBuffer(chunk) ? chunk : new Buffer(chunk, enc);
    this.bufferedBytes += buffer.length;
    this.buffers.push(buffer);

    var loopEnd = this.options.loopEnd * this.bytesPerSample;
    if(this.bufferedBytes <= loopEnd || this.unloopable) {
      this.push(buffer);
    } else if(this.bufferedBytes - buffer.length <= loopEnd) {
      var overflow = this.bufferedBytes - loopEnd;
      this.push(buffer.slice(0, buffer.length - overflow));
    }

    process.nextTick(cb);
  }
  _flush(cb) {
    if(!this.unloopable) {
      var buffer = Buffer.concat(this.buffers, this.bufferedBytes);

      var loopStart = this.options.loopStart * this.bytesPerSample;
      var loopEnd = this.options.loopEnd * this.bytesPerSample;
      var fadeDuration = this.options.fadeDuration * this.options.sampleRate * this.bytesPerSample;

      var loopSection = new Buffer(buffer.slice(loopStart, loopEnd));
      for(var i = 2; i <= this.options.loopAmount; i++) {
        this.push(loopSection);
      }

      var fadeSection = new Buffer(buffer.slice(loopStart, loopStart + fadeDuration));
      for(var x = 0; x < fadeSection.length; x += this.bytesPerSample) {
        for(var y = 0; y < this.options.channels; y++) {
          var offset = x + y * (this.options.bitDepth / 8);
          var value = fadeSection.readFloatLE(offset);
          value *= 1 - (x / fadeSection.length);
          fadeSection.writeFloatLE(value, offset);
        }
      }
      this.push(fadeSection);
    }

    process.nextTick(cb);
  }
}

module.exports = XivLooper;
