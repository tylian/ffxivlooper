"use strict";

var fs = require("fs");
var path = require("path");
var ogg = require("ogg");
var vorbis = require("vorbis");
var lame = require("lame");
var id3 = require("node-id3");
var util = require("util");

var XivLooper = require("./lib/XivLooper");
var argv = require('minimist')(process.argv.slice(2));

function parseIncludes(obj, dict) {
  var result = {};
  if(obj["include"] !== undefined) {
    var includes = obj["include"].split(", ");
    for(var i = 0; i < includes.length; i++) {
      result = util._extend(result, parseIncludes(dict[includes[i]], dict));
    }
  }
  
  result = util._extend(result, obj);
  delete result["include"];

  return result;
}

function convertFile(options) {
  return new Promise((resolve, reject) => {
    var oggDecoder = new ogg.Decoder();
    var lameEncoder = new lame.Encoder({ channels: 2, bitDepth: 32, float: true });

    console.time(`Converted ${options.name}`);

    oggDecoder.on("stream", stream => {
      var vorbisDecoder = new vorbis.Decoder();

      var comments = {};
      vorbisDecoder.on("comments", lines => {
        comments["vendor"] = lines.vendor;
        lines.forEach(line => {
          line = line.split("=");
          comments[line[0]] = line[1];
        });
      });

      vorbisDecoder.on("format", format => {
        var opt = format;
        opt["loopStart"] = parseInt(comments["LoopStart"], 10) || 0;
        opt["loopEnd"] = parseInt(comments["LoopEnd"], 10) || 0;
        opt["loopAmount"] = options.loops;
        opt["fadeDuration"] = options.fade;

        if(format["channels"] !== 2) {
          console.log(`Layered file ${options.name}: Moved to layered folder`);
          fs.renameSync(options.input, options.layered);
          writeStream.close();
          fs.unlinkSync(options.output);
          return resolve();
        } else {
          vorbisDecoder
            .pipe(new XivLooper(opt))
            .pipe(lameEncoder);
        }
      });

      vorbisDecoder.on("error", reject);
      stream.pipe(vorbisDecoder);
    });

    var readStream = fs.createReadStream(options.input);
    var writeStream = fs.createWriteStream(options.output);

    writeStream.on("finish", () => {
      console.timeEnd(`Converted ${options.name}`);
      resolve(options.output);
    });

    readStream.pipe(oggDecoder);
    lameEncoder.pipe(writeStream);
  });
}

var tags = fs.readFileSync(path.join(__dirname, "tags.json"), { "encoding": "utf8" });
tags = JSON.parse(tags);

var files = fs.readdirSync("input")
.map(file => {
  if(file.substr(-8, 8) != ".scd.ogg") {
    console.log(`Unknown file: ${file}`)
    return Promise.resolve();
  }

  file = file.substr(0, file.indexOf("."));
  return new Promise((resolve, reject) => {
    var options = {
      name: file,
      input: path.join(argv._[0] || "input", `${file}.scd.ogg`),
      output: path.join(argv._[1] || "output", `${file}.mp3`),
      layered: path.join(argv._[2] || "layered", `${file}.scd.ogg`),
      loops: argv.loops || 2,
      fade: argv.fade || 10
    };

    var promise;

    if(fs.existsSync(options.output)) {
      if(fs.statSync(options.output).size > fs.statSync(options.input).size / 2) {
        console.log(`Already exists: ${file}`);
        promise = Promise.resolve(options.output);
      } else {
        console.log(`Already exists (too small): ${file}`);
      }
    }

    if(promise == undefined) {
      promise = convertFile(options);
    }

    promise.then(result => {
        if(!result) {
          return;
        }

        var meta = util._extend({}, tags["*"]);
        var fileTags = parseIncludes(tags[file], tags);
        meta = util._extend(meta, fileTags);

        var success = id3.write(meta, result);
        if(success !== true) {
          throw new Error(success)
        }
      })
      .then(resolve)
      .catch(err => {
        console.log(`Unable to convert ${file}: ${err.message}`);
      });
  });
});

Promise.all(files).then(() => {
  console.log("Done!");
});
